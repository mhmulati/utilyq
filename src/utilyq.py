#!/usr/bin/env -S python3 -B

import sys
import math


PI = 3.141592
RRR = 6378.388

EPSILON = 0.001
EPSILON_LYSGAARD = 0.0001  # lysgaard
EPSILON_REL_GAP = 0.000001  # sprog
EPSILON_ABS_GAP = 0.00000001  # sprog
EPSILON_INT_TOL = 0.00001

colors = ['purple', 'peru', 'deepskyblue4', 'hotpink', 'indigo', 'lightsalmon', 'olivedrab3', 'yellow4']
default_color = 'purple'


def pass_(*s):
	pass


def verb_(*s, f=sys.stderr):
	print(*s, file=f)


def msg_(*s, f=sys.stderr):
	print(*s, file=f)


def warn(*s, f=sys.stderr):
	print(*s, file=f)


sellog = False


def log_(*s, sel=False, f=sys.stderr):
	global sellog
	if not sellog or (sellog and sel):
		print(*s, file=f)


verb = pass_
msg = pass_
log = pass_


def setup_msgs(enable_verb=False, enable_msg=False):
	if enable_verb:
		verb__ = verb_
	else:
		verb__ = pass_

	if enable_msg:
		msg__ = msg_
	else:
		msg__ = pass_

	return verb__, msg__


def setup_log(enable_log=False, sellog_=False):
	if enable_log:
		log__ = log_
	else:
		log__ = pass_

	return log__, sellog_

"""
To be used accordingly TSPLIB95
"""
def nint(x):
	return math.floor(x + 0.5)  # (int) (x + 0.5)


def dist(xi, yi, xj, yj, use_nint=True, num_dec_places_real_dist=0):
	verb('xi ' + str(xi) + ' yi ' + str(yi) + ' xj ' + str(xj) + ' yj ' +str(yj))
	if use_nint:
		return nint(math.sqrt((xj - xi) * (xj - xi) + (yj - yi) * (yj - yi)))
	else:
		if num_dec_places_real_dist == 0:
			return math.sqrt((xj - xi) * (xj - xi) + (yj - yi) * (yj - yi))
		else:
			return round(math.sqrt((xj - xi) * (xj - xi) + (yj - yi) * (yj - yi)), num_dec_places_real_dist)


def geocoord_to_rad(geocoord):
	global PI
	degrees = int(geocoord)
	minutes = geocoord - degrees
	return PI * (degrees + 5.0 * minutes / 3.0) / 180.0


def mvzeros(i, num_digits=4):
	if i < 0:
		return str(i)
	num_left_zeros = max(num_digits - len(str(i)), 0)
	return '0' * num_left_zeros + str(i)


def mvzeros_(i, num_digits=4):
	return mvzeros(i, num_digits)


def mvzeros2(i):
	return mvzeros_(i, 2)


def get_color(i=0, type_=None):
	# http://www.graphviz.org/doc/info/colors.html
	# Colors removed: 'turquoise','cyan'
	global colors
	if i == 0 or type_ in ['S', 'T', 'D']:
		return 'black'
	else:
		return colors[(i - 1) % len(colors)]


def get_grey_color(i):
	# http://www.graphviz.org/doc/info/colors.html
	return 'grey' + str((i * 20) % 100)


def get_shape(i, type_=None):
	if i == 0 or type_ in ['S', 'T', 'D']:
		return 'square'
	elif type_ in ['B']:
		return 'triangle'
	elif type_ in ['C', 'L', '', None]:
		return 'circle'


def get_penwidth(i):
	if i == 0:
		return 3
	else:
		return 1


def get_edge_style(x):
	if x is None:
		return 'solid'

	if 0.5 - EPSILON_INT_TOL <= x <= 0.5 + EPSILON_INT_TOL:
		return 'dashed'

	return 'solid'


def fail(msg__='Error: fail: generic'):
	sys.exit(msg__)


def fail_if_(fail_, msg__='Error: fail_if_: generic'):
	if fail_:
		fail(msg__)


def proceed_only_if_(proceed, msg__='Error: proceed_only_if_: generic'):
	if not proceed:
		fail(msg__)


def turn_true_only_once_or_fail(flag, msg__='Error: turn_true_only_once_or_fail: generic'):
	if not flag:
		return True
	else:
		fail(msg__)


def turn_true_only_once_or_warn(flag, msg__='Error: turn_true_only_once_or_warn: generic'):
	if not flag:
		return True
	else:
		warn(msg__)


def remprefix(s: str, prefix: str) -> str:
	if s.startswith(prefix):
		return s[len(prefix):]
	else:
		return s[:]


def remsuffix(s: str, suffix: str) -> str:
	if suffix and s.endswith(suffix):
		return s[:-len(suffix)]
	else:
		return s[:]
