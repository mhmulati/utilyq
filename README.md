# utilyq

The `utilyq` stands for utility python package/module of Mauro Henrique Mulati.

This software is licensed under [Mozilla Public License 2.0][mpl2] (MPL2). Contributions will be accepted under the same license. The MPL2 is a [copyleft][copyleft] license and one of its requirements is that the modified files must be made available if the sofware is public distributed. You may want to see its [FAQ][mpl2faq]. More on [license differences][licdiff].

[mpl2]: https://www.mozilla.org/en-US/MPL/2.0/
[mpl2faq]: https://www.mozilla.org/en-US/MPL/2.0/FAQ/
[copyleft]: https://en.wikipedia.org/wiki/Copyleft#Strong_and_weak_copyleft
[licdiff]: http://oss-watch.ac.uk/apps/licdiff/

[apache2]: http://www.apache.org/licenses/LICENSE-2.0
[mit]: http://opensource.org/licenses/MIT

